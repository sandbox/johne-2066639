Atom Captcha Extra

Description:
This Extra module performs tasks that are not safe to include directly in Features code due to the possibility of overwrites.
The Atom Captcha Extra module adds default captcha settings that are not handled natively in Features.

Caveats:

